fn even? (arg)
    (arg % 2 as (typeof arg)) == 0 as (typeof arg)

fn odd? (arg)
    (arg % 2 as (typeof arg)) == 1 as (typeof arg)

spice static-even? (arg)
    even? (arg as usize)

spice static-odd? (arg)
    odd? (arg as usize)

fn isqrt (n)
    let T = (typeof n)
    static-assert (T < integer) "isqrt only works with integers"
    static-if (-1 as T < 0 as T) "isqrt only works for unsigned inputs"

    shift := 2 as T

    let shift =
        loop (shift shifted = shift (n >> shift))
            if (shift != 0 and shifted != n)
                shift := shift + 2
                repeat shift
                    n >> shift
            else
                break
                    shift - 2

    loop (result shift = (0 as T) shift)
        result := result << 1
        result* := result + 1
        let new-result =
            ? (result* * result* <= n >> shift) result* result
        if (shift > 0)
            repeat new-result
                shift - 2
        else
            break new-result


spice static-isqrt (n)
    define T
        match ('typeof n)
        case i8
            let result =
                isqrt (n as i8)
            spice-quote result
        case u8
            let result =
                isqrt (n as u8)
            spice-quote result
        case i16
            let result =
                isqrt (n as i16)
            spice-quote result
        case u16
            let result =
                isqrt (n as u16)
            spice-quote result
        case i32
            let result =
                isqrt (n as i32)
            spice-quote result
        case u32
            let result =
                isqrt (n as u32)
            spice-quote result
        case i64
            let result =
                isqrt (n as i64)
            spice-quote result
        case u64
            let result =
                isqrt (n as u64)
            spice-quote result
        case usize
            let result =
                isqrt (n as usize)
            spice-quote result
        default
            error "Argument for isqrt needs to be of a base integer type"

run-stage;

inline even? (arg)
    static-if (constant? arg)
        static-even? (arg as usize)
    else
        even? arg

inline odd? (arg)
    static-if (constant? arg)
        static-odd? (arg as usize)
    else
        odd? arg

unlet static-even?
unlet static-odd?

inline isqrt (n)
    static-if ((constant? n) and (typeof n) < integer)
        static-isqrt n
    else
        isqrt n

unlet static-isqrt

fn factorial (n)
    let one =
        1 as (typeof n)
    loop (i result = n one)
        if (i > one)
            _ (i - one) (result * i)
        else
            break result

spice over (n k)
    let op =
        if (('typeof n) < integer and ('typeof k) < integer)
            do //
        else
            do /
    spice-quote
        op
            op
                factorial n
                factorial (- n k)
            factorial k

define-infix> 550 over

locals;

