using import testing
using import ..argless

inline check-argless-nums (op a b c)
    test
        and
            a ==
                op 1
            b ==
                op 1 2
            c ==
                op 1 2 3

check-argless-nums (do +) 1 3 6
check-argless-nums (do *) 1 2 6
check-argless-nums (do |) 1 3 3
check-argless-nums (do &) 1 0 0

inline check-argless-str (op a b c)
    test
        and
            a ==
                op "a"
            b ==
                op "a" "b"
            c ==
                op "a" "b" "c"

check-argless-str (do ..) "a" "ab" "abc"
