using import testing

using import ..types

let generic-name =
    generic-name "vec" 3 f32

test (generic-name == "vec<3 f32>")

mono MonoTest

test
    ==
        MonoTest as type
        typeof MonoTest

test
    constant? MonoTest

let int-or-string =
    variant i32 string

let int-variant string-variant =
    int-or-string 1
    int-or-string "test"

test
    'is? int-variant i32

test
    do
        try
            1 ==
                'get int-variant i32
        except ()
            error "Varant does not contain value of type i32"

test
    'is? string-variant string

test
    do
        try
            "test" ==
                'get string-variant string
        except ()
            error "Varant does not contain value of type string"

do
    typedef Proxy

    let x y z =
        proxyof Proxy 1
        proxyof Proxy 5
        proxyof Proxy "Test"

    test
        ==
            typeof x
            typeof y

    print x y z
    print
        va-map 'value x y z

do
    proxydef Proxy

    let x y =
        Proxy 1
        Proxy 5

    test
        ==
            typeof x
            typeof y

    test
        ==
            typeof x
            Proxy.type i32

    print x y
    print
        va-map 'value x y
    print
        Proxy.type i32

