using import testing

using import ..files

do
    local write-failed = false
    let file =
        try
            File "testing/files/test" "wb"
        except ()
            error "Opening file failed"

    try
        'write file (local i32 1)
        'write file (local u32 2:u32)
    except ()
        error "Writing to file failed"

do
    local read-failed = false
    let file =
        try
            File "testing/files/test" "rb"
        except ()
            error "Opening file failed"

    local a : i32
    local b : u32

    try
        'read file a
        'read file b
        _;
    except ()
        error "Reading from file failed"

    test
        and
            a == 1
            b == 2:u32

