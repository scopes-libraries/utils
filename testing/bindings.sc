using import testing
using import enum

using import ..bindings

let remover = (prefix-remover "my")

test ((remover "MyStructName") == "StructName")
test ((remover "myFunctionName") == "functionName")
test ((remover "MY_CONST_NAME") == "CONST_NAME")
test ((remover "MYtype") == "type")

test-error
    remover "myinvalid"

test-error
    remover "MYINVALID"

define scope
    let scope_value = 123
    fn scope_function (val)
        print "This is" val
    locals;

let const-scope =
    'remove-prefix scope "scope"

test
    constant? scope

let scope =
    copy-bindings scope (prefix-remover "scope") (Scope)

run-stage;

scope.function scope.value

enum LibEnum
    LibEnum_Field1
    LibEnum_Field2
    LibEnum_Field3

copy-bindings LibEnum (prefix-remover "LibEnum")

run-stage;

print LibEnum.Field1

define scope
    let w x y z = 0 1 2 3
    locals;

inline check-result (z x y ...)
    test
        and
            x == 1
            y == 2
            z == 3

check-result
    unpack scope
