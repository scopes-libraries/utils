using import testing

using import ..va

let a b c d e rest... = (va-reverse 1 2 3 4 5)
test
    and
        a == 5
        b == 4
        c == 3
        d == 2
        e == 1
        va-empty? rest...

test
    ==
        (va-map-functions (do +) (do *)) 1 2 3

inline test-mappend (x y z e)
    test
        x == 2
    test
        y == 3
    test
        z == 1
    test
        e == "E"

test-mappend
    va-mappend unpack
        tupleof (z = 1)
        tupleof (x = 2) (y = 3)
        tupleof;
        tupleof "E"

let a b c rest... =
    va-range 3 9 2

test
    and
        a == 3
        b == 5
        c == 7
        va-empty? rest...

test
    ==
        va-type@ u32 1 2:u32 3:f32
        2:u32

let a b c rest... =
    va-fill 3 "a"
test
    and
        a == "a"
        b == "a"
        c == "a"
        va-empty? rest...

test
    1 ==
        va-pos 2
            \ 1 2 3
test
    1 ==
        va-pos 2
            \ 1 2 3 2 1
test
    3 ==
        va-pos 2
            \ 5 4 3 2 1

test
    none?
        va-pos 0
            \ 5 4 3 2 1

let a b c rest... =
    va-rest 1
        \ 0 1 2 3
test
    and
        a == 1
        b == 2
        c == 3
        va-empty? rest...

let a b c rest... =
    va-slice 1 4
        \ 0 1 2 3 4
test
    and
        a == 1
        b == 2
        c == 3
        va-empty? rest...


let a b c d e rest... =
    va-cycle 2 1 2 3 4 5
test
    and
        a == 4
        b == 5
        c == 1
        d == 2
        e == 3
        va-empty? rest...

let a b c d e rest... =
    va-cycle -2 1 2 3 4 5
test
    and
        a == 3
        b == 4
        c == 5
        d == 1
        e == 2
        va-empty? rest...

let a b c d e rest... =
    va-cycle 7 1 2 3 4 5
test
    and
        a == 4
        b == 5
        c == 1
        d == 2
        e == 3
        va-empty? rest...

let a b c d e f rest... =
    va-swap 2 4
        \ 0 1 2 3 4 5

test
    and
        a == 0
        b == 1
        c == 4
        d == 3
        e == 2
        f == 5
        va-empty? rest...

#

    print
        va-find (fn (x) (x < 5))
            \ 10 9 8 7 6 5 4 3 2 1 0

    print
        va-sort (do <)
            \ 5 4 3 6 7 2 1

    print
        va-remove-doubles 1 1 1 1 2 2 3 3 4 5 6 6 6 6 6 7 8 8

    print
        va-remove-doubles
            va-sort (do >)
                \ 1 2 1 2 2 1 3 2 3 2 2 1 2 3 1 2 3 4 5 2 3 1


    fn even? (arg)
        and
            (typeof arg) < integer
            (arg % 2) == 0
    print # 3 5 7
        va-remove even? 2 3 4 5 6 7 8

    print # 2 1 3 1 1 2 1
        va-group-counts
            \ 1 1 2 3 3 3 4 5 6 6 7

