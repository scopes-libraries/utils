using import testing

using import ..casing

test (uppercase? "ABC")
test (not (lowercase? "ABC"))
test (lowercase? "xyz")
test (not (uppercase? "xyz"))
test (uppercase? "ABC123")
test (not (lowercase? "ABC123"))
test (lowercase? "xyz123")
test (not (uppercase? "xyz123"))
test (uppercase? "123")
test (lowercase? "123")
test (not (uppercase? "ABCxyz123"))
test (not (lowercase? "ABCxyz123"))

test ((uppercase "ABCxyz123") == "ABCXYZ123")
test ((lowercase "ABCxyz123") == "abcxyz123")

test ((camel "hello_world") == "HelloWorld")
test ((camel "Hello-World" 45:i8) == "HelloWorld")
test ((uncamel-uppercase "HelloWorld") == "HELLO_WORLD") # C preprocessor constants
test ((uncamel-lowercase "HelloWorld" 45:i8) == "hello-world") # lisp-like
test ((uncamel-lowercase "ABCtest") == "abctest") # no difference from large to small

