using import testing

using import ..va
using import ..math
using import ..functional

inline test-sub (a b)
    a - b

let subtract-from-1 =
    curry test-sub 1
let subtract-from-3 =
    curry test-sub 3

test
    and
        (subtract-from-1 1) == 0
        (subtract-from-3 2) == 1

let subtract-1 =
    curry test-sub
        b = 1

test
    (subtract-1 2) == 1

test
    (subtract-1 (a = 0)) == -1

let something =
    inline (a b c)
        a + b * c

let something2 =
    curry something (b = 2)

test
    (something2 1 3) == 7

test
    (something2 (c = 3) 1) == 7

let nn =
    compose (inline (x) (not x)) none?

test
    nn '()

let s =
    complement <

test
    s 2 1

fn print-and-return (to-print return...)
    print to-print
    return...

let print-and-sum =
    compose (do +) print-and-return

test
    == 10
        print-and-sum "Hello" 1 2 3 4

fn x+y (x y)
    x + y

fn ab-to-xy (a b)
    return
        x = a
        y = b

let a+b =
    compose x+y ab-to-xy

test
    ==
        a+b
            a = 1
            b = 2
        3

#

    fn call-and-args (f ...)
        return
            f ...
            ...

    let reverse-sum-and-print =
        compose print (curry call-and-args +) va-reverse

    reverse-sum-and-print 1 2 3 4 5

test (all even? 2 4 6)
test (not (all even? 1 2 3 4))
test (any odd? 1 2 3 4)
test (not (any odd? 2 4 6))

test ((count even? (range 10)) == 5)

let avg =
    compose-apply (do /) (do +) va-countof

test
    ==
        3.0
        avg 1 2 3 4 5

