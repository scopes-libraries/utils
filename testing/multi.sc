using import testing

using import ..multi

fn print-ret (val)
    print val
    val
let a b c =
    print-ret 1
    print-ret 2
    print-ret 3

test
    and
        a == 1
        b == 2
        c == 3

let print-rets =
    make-multi print-ret

let x y z =
    print-rets 1 2 3

test
    and
        a == 1
        b == 2
        c == 3

