using import ..iteration

iter (list 1 2 3) print "iter"
iter-tuples 1 (list 1 2 3) print "iter-tuples 1"
iter-tuples (list 1 2 3) print "iter-tuples"
iter-tuples 2 (list 1 2 3) print "iter-tuples 2"
iter-tuples 3 (list 1 2 3 4 5) print "iter-tuples 3"
iterate (va-each 1 2 3) print "iterate"

