using import testing

using import ..transcendental

let accuracy = 18

let exp* sin* cos* exph* sinh* cosh* =
    make-exp accuracy
    make-sin accuracy
    make-cos accuracy
    make-exph accuracy
    make-sinh accuracy
    make-cosh accuracy

inline test-similar (a b delta msg)
    print a b
    test ((a - b) < delta) msg

inline tests (i)
    print "next:" i
    test-similar
        exp i
        exp* i
        0.1
        "exp"
    test-similar
        sin i
        sin* i
        0.1
        "sin"
    test-similar
        cos i
        cos* i
        0.1
        "cos"
    test-similar
        sinh i
        sinh* i
        0.1
        "sinh"
    test-similar
        cosh i
        cosh* i
        0.1
        "cosh"
    print;


tests 0.0
tests 1.0
tests 2.0
tests pi
tests (2 * pi)

