using import testing

using import ..transitive

test
    < 1 2 3

test
    not
        < 1 2 3 5 4

test
    not
        < 1 2 3 5 5

test
    <= 1 2 3 5 5

test
    not
        >= 1 2 3 5 5

test
    == 1 1 1 1 1

test
    == 2 2 2

test
    not
        == 3 3 3 1 3

test
    != 3 3 3 1 3

test
    != 1 2 3 4 5

test
    not
        != 1 1 1 1

