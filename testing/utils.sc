using import testing
using import struct

using import ..init

ign (print (unimplemented-function (a + b)))

ign
    some
        deeply
            nested
                body


let x =
    or none 1 none none 3
test (x == 1)



#
    define use-graphics false
    define use-terminal true
    define use-sound false

    let interface =
        some
            if use-graphics 'graphical-interface
            if use-terminal 'terminal
            if use-sound 'sound-engine

    dump interface

    if (none? interface)
        error! "No valid interface found"
    print "Setting up interface successful!"
    #todo next:
        text-program.run-with-text-function (get-text-function interface)

let x = 1
dbg x

let a b c = 1 2 3

let str a b c =
    dbg "debug" c b a

dump str a b c

test (str == "debug")

dbg a b c

let vecs... =
    vectorof i32 1 2 3
    vectorof i32 3 2 1

test
    all?
        ==
            + vecs...
            vector-map (do +) vecs...

let a b c = 1 2 3

let c b a =
    keyed a b c

test
    and
        a == 3
        b == 2
        c == 1

struct StructWithLongNames
    long-name : i32
    longer-name : i32
    even-longer-name : i32
    some-other-name : i32

let long-name = 1
let longer-name = 2
let even-longer-name = 3

local test-struct : StructWithLongNames
    some-other-name = (1 + 2 - 3) # for non-variable names, you can still use the old keyword value syntax
    # the folliwing replaces (key = value) for all the names
    keyed even-longer-name long-name longer-name

test
    5 ==
        pos "hello world" 32:i8

test
    11 ==
        pos "hello_world" 32:i8

test
    test-struct.long-name == 1
    test-struct.longer-name == 2
    test-struct.even-longer-name == 3
    test-struct.some-other-name == 0

test-compiler-error
    keyed 1 2 3

let z = 0
test-compiler-error
    keyed (x = 1) (+ 1 2 3) z

typedef+ i32
    inline test-method (self)
        print "Calling test method:" self

do
    method-fn test-method
    test-method 1

do
    method-inline test-method
    test-method 2



let x = (do-file "testing/utils/prog.sc")
let y = 2

test
    x == 3
test
    y == 2



let x = (embed-file "testing/utils/prog.sc")

test
    x == 3
test
    y == 1

