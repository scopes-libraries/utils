using import testing
using import ..padding

inline same-padding-test (T)
    test
        (sizeof T) == (sizeof (aligned T))

let test-tuple =
    tuple i8 u8 u16 u32 u64

va-map same-padding-test u16 i32 u64 f16 f32 test-tuple

inline size== (a b)
    ==
        sizeof a
        sizeof b

test
    size== test-tuple
        aligned
            tuple u8 u16 u32 u64


