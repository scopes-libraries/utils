using import testing
using import ..math

test
    even? 2

test
    odd? 3

test
    constant?
        odd? 3

test
    (factorial 5) == 120

test
    (isqrt 4) == 2

test
    (isqrt 9) == 3

test
    (isqrt 6) == 2

test
    0 over 0 == 1

test
    16 over 16 == 1

test
    4 over 3 == 4

test
    constant? (isqrt 2)
