using import testing
using import struct
using import ..pointer-count

struct Test plain
    value : i32

let PC =
    PointerCount Test

let p count =
    unpack (none as (PointerCount Test))

test
    ==
        typeof p
        mutable pointer Test

test
    count == 0

local test1 : Test
    value = 1

let p count =
    unpack (test1 as (PointerCount Test))

test
    ==
        typeof p
        mutable pointer Test

test
    count == 1

local test2 =
    arrayof Test
        typeinit
            value = 1
        typeinit
            value = 2
        typeinit
            value = 3

let p count =
    unpack (test2 as (PointerCount Test))

test
    ==
        typeof p
        mutable pointer Test

test
    count == 3

