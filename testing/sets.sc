using import testing
using import ..sets

inline test-results (results...)
    inline (tests...)
        va-lifold none
            inline (i k v)
                test
                    v == (va@ i tests...)
            results...

let _123 _12 _13 =
    setof i32 1 2 3
    setof i32 1 2
    setof i32 1 3

inline test (a)
    test a

va-map test
    not
        _123 < _12
    not
        _123 <= _12
    not
        _123 == _12
    _123 >= _12
    _123 > _12

va-map test
    _12 < _123
    _12 <= _123
    not
        _12 == _123
    not
        _12 >= _123
    not
        _12 > _123

va-map test
    not
        _123 < _123
    _123 <= _123
    _123 == _123
    _123 >= _123
    not
        _123 > _123

va-map test
    not
        _12 < _13
    not
        _12 <= _13
    not
        _12 == _13
    not
        _12 >= _13
    not
        _12 > _13

test
    _12 .. _13 == _123
test
    _12 .. _12 == _12

