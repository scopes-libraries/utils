using import .init
using import .va

spice static-default-alignment (count)
    count := count as usize
    loop (i = 2)
        if (i < count)
            i * 2
        else
            break i

run-stage;

@@ memo
inline padded (T size)
    let pad-count pad-type =
        static-if (size % 8 == 0)
            _ (size // 8) u64
        elseif (size % 4 == 0)
            _ (size // 4) u32
        elseif (size % 2 == 0)
            _ (size // 2) u16
        else
            _ size u8
    let storage-type = (tuple T (va-fill pad-count pad-type))
    typedef (generic-name "Padded" T size) : storage-type
        inline __typecall (ty args...)
            bitcast
                tupleof (T args...) (va-fill pad-count (0 as pad-type))
                this-type

        inline __as (vT T)
            inline (value)
                bitcast
                    tupleof value (va-fill pad-count (0 as pad-type))
                    this-type
        inline __imply (vT T_inner)
            static-if (T == T_inner)
                inline (value)
                    (storagecast value) @ 0

        inline __getattr (self attr)
            T.__getattr
                (storagecast self) @ 0
                attr

        inline __methodcall (symbol self args...)
            T.__methodcall symbol
                (storagecast self) @ 0
                args...



inline aligned (T size)
    let type-size =
        sizeof T
    let alignment =
        static-if (none? size)
            static-default-alignment type-size
        else
            size as i32
    type-size := type-size as i32
    let padding =
        alignment - ((type-size - 1) % alignment) - 1
    padded T padding

inline align (value size)
    (aligned (typeof value) size) value

inline pad (value size)
    value as (padded (typeof value) size)

inline unpad (value)
    (storagecast value) @ 0

locals;
