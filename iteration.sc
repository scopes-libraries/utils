using import .va

fn tuple-mapper (element-count include-self? function iter start tuple-size args...)
    fn (...)
        if (tuple-size == 1)
            let loop (current) = start
            iter
                label (next elements...)
                    function 
                        (va-join args...)
                            (va-join (if (none? element-count) elements...)
                                     (else (va-slice 0 element-count elements...))) ...
                    loop next
                \ return current
        else
            #print ...
            let loop (current) = start
            iter
                label (next elements...)
                    (tuple-mapper element-count include-self? function iter (if include-self? current) (else next) (tuple-size - 1)
                      ((va-join args...)
                        (if (none? element-count) elements...)
                            (else (va-slice 0 element-count elements...)))) ...
                    loop next
                \ return current

fn iterate (count ...)
    """"General function to iterate over objects in a generator

        Usage of `iterate`:
        * `iterate gen method args...` => call method on all elements of gen
            - `method` is `fn (element args...)`
        * `iterate n gen method args...` => call method of all n-tuples in gen
            - `method` is `fn (element_1 ... element_n args...)`
            - every tuple only appears once:
                `iterate 2 (va-each 1 2 3) f` same as `do (f 1 2) (f 1 3) (f 2 3)`
        * `iterate n true gen method args...` => also include same-element-tuples
            - Example: `iterate 2 (va-each 1 2 3) f`
                same as `do (f 1 1) (f 1 2) (f 1 3) (f 2 2) (f 2 3) (f 3 3)`
        * `iterate gen m method args...` => specify argument count
            - `method` is `fn (element_arg_1 ... element_arg_m args...)`
            - if `m` is none, `m` is the return argument count of f
            - Example: `iterate (enumerate (va-each 1 2 3)) none f args...`
                `do (f 0 1) (f 1 2) (f 2 3)` (here `none` could also be `2`)
        * `iterate n s gen m method args...` => most general form
            - `method` is `fn (elmenet_1_arg_1 ... element_1_arg_m ... element_n_arg_1 ... element_n_arg_m args...)`
            
    let count include-self? gen method ... =
        if (integer? count)
            let include-self? ... = ...
            if ((typeof include-self?) == bool)
                _ count include-self? ...
            else
                _ count false include-self? ...
        else
            _ 1 false count ...
    let element-count method args... =
        if ((integer? method) or (none? method))
            _ method ...
        else
            _ 1 method ...
    let iter start = ((gen as Generator))
    (tuple-mapper element-count include-self? method iter start count) args...

fn iter-tuples (count ...)
    let count gen f args... =
        if (integer? count)
            _ count ...
        else
            _ 2 count ...
    let iter start = ((gen as Generator))
    (tuple-mapper 1 false f iter start count) args...

fn iter-tuples-all (count ...)
    let count gen f args... =
        if (integer? count)
            _ count ...
        else
            _ 2 count ...
    let iter start = ((gen as Generator))
    (tuple-mapper none false f iter start count) args...

fn iter-tuples-include-self (count ...)
    let count gen f args... =
        if (integer? count)
            _ count ...
        else
            _ 2 count ...
    let iter start = ((gen as Generator))
    (tuple-mapper 1 true f iter start count) args...

fn iter-tuples-include-self-all (count ...)
    let count gen f args... =
        if (integer? count)
            _ count ...
        else
            _ 2 count ...
    let iter start = ((gen as Generator))
    (tuple-mapper none true f iter start count) args...

#fn tuples (gen count)
    let count =
        if (none? count) 2
        else count
    let iter start = ((gen as Generator))
    (tuple-mapper f iter start count)

fn iter (gen f ...)
    let iter start = ((gen as Generator))
    let loop (next) = start
    iter
        label (next element)
            f element ...
            loop next
        \ return next

fn iter-all (gen f ...)
    let iter start = ((gen as Generator))
    let loop (next) = start
    iter
        label (next elements...)
            f ((va-join elements...) ...)
            loop next
        \ return next

do
    let iter iter-all iter-tuples iter-tuples-all iter-tuples-include-self iter-tuples-include-self-all iterate
    locals;

