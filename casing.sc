let char-a = 97:i8
let char-z = 122:i8
let char-A = 65:i8
let char-Z = 90:i8

define case-diff
    char-a - char-A

run-stage;

inline uppercase? (v)
    'uppercase? v

inline lowercase? (v)
    'lowercase? v

inline uppercase (v)
    'uppercase v

inline lowercase (v)
    'lowercase v

inline camel (...)
    'camel ...

inline uncamel-uppercase (...)
    'uncamel-uppercase ...

inline uncamel-lowercase (...)
    'uncamel-lowercase ...

fn char-uppercase? (c)
    and
        <= char-A c
        <= c char-Z

fn char-lowercase? (c)
    and
        <= char-a c
        <= c char-z

fn char-uppercase (c)
    if (char-lowercase? c)
        c - case-diff
    else c

fn char-lowercase (c)
    if (char-uppercase? c)
        c + case-diff
    else c

'set-symbols i8
    uppercase? = char-uppercase?
    lowercase? = char-lowercase?
    uppercase = char-uppercase
    lowercase = char-lowercase

fn string-uppercase? (s)
    for i in (range (countof s))
        c := s @ i
        if (char-lowercase? c)
            return false
    true

fn string-lowercase? (s)
    for i in (range (countof s))
        c := s @ i
        if (char-uppercase? c)
            return false
    true

inline string-case (char-case)
    fn (s)
        let len = (countof s)
        let str =
            alloca-array i8 len
        for i in (range len)
            let c =
                s @ i
            str @ i =
                char-case c
        sc_string_new str len

fn string-camel (s delimiter)

fn... string-camel (s, delimiter : i8 = 95:i8)
    let len = (countof s)
    let str =
        alloca-array i8 len
    let _ camels =
        fold (start camels = true 0) for i in (range len)
            c := s @ i
            if (c == delimiter)
                _ true
                    camels + 1
            else
                let c =
                    if start
                        char-uppercase c
                    else
                        char-lowercase c
                str @ (i - camels) = c
                _ false camels
    sc_string_new str (len - camels)

inline string-uncamel (char-case)
    fn... (s, delimiter : i8 = 95:i8)
        let len = (countof s)
        let delimiter =
            string (& (local i8 delimiter)) 1
        let str =
            fold (str last-small = "" false) for i in (range len)
                c := s @ i
                let uppercase? =
                    uppercase? c
                let c =
                    char-case c
                let c =
                    string (& (local i8 c)) 1
                _
                    if (last-small and uppercase?)
                        str .. delimiter .. c
                    else
                        str .. c
                    not uppercase?
        str

let string-uppercase =
    string-case char-uppercase
let string-lowercase =
    string-case char-lowercase

'set-symbols string
    uppercase? = string-uppercase?
    lowercase? = string-lowercase?
    uppercase = string-uppercase
    lowercase = string-lowercase
    camel = string-camel
    uncamel-uppercase =
        string-uncamel char-uppercase
    uncamel-lowercase =
        string-uncamel char-lowercase

do
    let uppercase? lowercase? uppercase lowercase
    let camel uncamel-lowercase uncamel-uppercase
    locals;

