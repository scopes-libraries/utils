using import .types

define c
    include "stdio.h"

from c.typedef let FILE
from c.extern let fopen fclose fwrite fread

mono FileOpenError
mono FileWriteError
mono FileReadError

typedef File :: (mutable pointer FILE)
    fn __typecall (self path mode)
        let file =
            fopen path mode
        if (file == null)
            raise FileOpenError
        bitcast file this-type

    inline __drop (self)
        fclose (storagecast self)
        _;

    fn write (self thing)
        if ((fwrite &thing (sizeof (typeof thing)) 1 (storagecast (view self))) < 1)
            raise FileWriteError

    fn write-string (self string)
        for i in (range (countof string))
            local char : i8 =
                string @ i
            write self char

    fn read (self thing)
        if ((fread &thing (sizeof (typeof thing)) 1 (storagecast (view self))) < 1)
            raise FileReadError
        thing

do
    let File FileOpenError FileWriteError FileReadError
    locals;

