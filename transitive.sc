inline make-transitive (f)
    """"Creates a variadic function from a transitive binary function.
    inline (args...)
        va-rfold none
            inline "#hidden" (key val arg)
                static-if (none? arg)
                    val
                else
                    if (f val arg)
                        val
                    else
                        return false
            args...
        true


let == <= < > >= =
    make-transitive ==
    make-transitive <=
    make-transitive <
    make-transitive >
    make-transitive >=

let != =
    inline (args...) (not (== args...))

locals;

