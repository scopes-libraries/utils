These are utilities for use with the [scopes](http://scopes.rocks) programming language.

These features are intended to bypass restrictions of core.
Some if them would be useful to be included in scopes core.

# Library

This library consists of these libraries:
* `import utils`
* `import utils.va`
* `import utils.functional`
* `import utils.casing`
* `import utils.math`
* `import utils.files`
* `import utils.padding`

## Utils

Just some random utilities for different use cases.

## VA

A bunch of powerful operations to deal with variadics.

## Functional

A few functional programming helpers, which are most useful for compile time things.

## Casing

Just functions to test and convert the casing of characters and strings.

## Math

A few simple, but useful mathematical functions.

## Files

A wrapper around C files form stdio using constructors and destructors.

## Padding

Utilities to ensure correct Padding by creating padded types.  Most useful for data on the GPU.

# Maintainer

E-Mail: Krapohl.f@gmx.de

Freenode IRC: porky11

matrix: p11:matrix.org

Discord: porky11#6951

Tox: F3AA4C3ECBF041E468230545D38FD27C8E82B81360C424EC342D225F700C1B2FE918A41D1BF8

[Gitlab](https://gitlab.com/porky11)


