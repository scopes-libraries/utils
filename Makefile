.RECIPEPREFIX+=*

SCOPES_INSTALL_DIRECTORY=/usr/local/lib/scopes

.PHONY: install test

#check: core.sc
#    scopes core.sc

test:
* scopes testing/test.sc

install:
* rm -f $(SCOPES_INSTALL_DIRECTORY)/utils
* ln -s $(shell pwd) $(SCOPES_INSTALL_DIRECTORY)/utils

