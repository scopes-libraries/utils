using import .init

""""A method, which allows creating most transcendentals
@@ memo
inline make-trans (start step alt accuracy)
    static-assert (start == 0 or start == 1) "Start needs to be zero or one"
    static-assert (step == 1 or step == 2) "Step needs to be one or two"
    fn (x)
        let T = (typeof x)
        let zero one =
            0 as T
            1 as T

        let first =
            va-lfold one
                inline (k v result)
                    result * x
                va-range start

        let x* =
            va-lfold one
                inline (k v result)
                    result * x
                va-range step

        let result =
            fold (result next fac pos = zero first one true) for i in (range 0 accuracy)
                let add =
                    next * fac
                _
                    if pos
                        result + add
                    else
                        result - add
                    next * x*
                    va-lfold fac
                        inline (k v result)
                            result /
                                start + (i * step + v + 1) as T
                        va-range step
                    pos ^ alt
        result

inline make-exp (accuracy)
    make-trans 0 1 false accuracy

inline make-sin (accuracy)
    make-trans 1 2 true accuracy

inline make-cos (accuracy)
    make-trans 0 2 true accuracy

inline make-exph (accuracy)
    make-trans 0 1 true accuracy

inline make-sinh (accuracy)
    make-trans 1 2 false accuracy

inline make-cosh (accuracy)
    make-trans 0 2 false accuracy

locals;
