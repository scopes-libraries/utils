using import Set
using import .functional

inline switch (f)
    inline (a b)
        f b a

fn subset? (subset set)
    for val in subset
        if (not ('in? set val))
            return false
    true

inline binop (f)
    inline (T U)
        static-if (T == U) f

typedef+ Set
    let __<= __< __> __>= =
        va-map binop
            subset?
            inline (a b)
                and
                    subset? a b
                    not (subset? b a)
            inline (a b)
                and
                    subset? b a
                    not (subset? a b)
            switch subset?

    let __.. =
        binop
            fn (set1 set2)
                local result : (typeof set1)
                for k in set1
                    'insert result k
                for k in set2
                    'insert result k
                result

    let __== =
        binop
            fn (set1 set2)
                for val in set1
                    if (not ('in? set2 val))
                        return false
                return
                    ==
                        countof set1
                        countof set2

    fn clone (self)
        local result : (typeof self)
        for k in (view self)
            'insert result k
        result

    fn __hash (self)
        local result : hash 0
        for k in self
            (storagecast result) +=
                storagecast
                    hash k
        result

inline setof (T values...)
    local set : (Set T)
    va-lfold none
        inline (k v)
            'insert set
                v as T
        values...
    set

if main-module?

do
    let setof
    locals;
