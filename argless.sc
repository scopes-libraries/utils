inline make-argless (op default)
    """"Takes a binary operator, that takes two or more arguments, and creates versions for one argument and optionally also zero arguments.
        The one argument version just returns the value itself.
        In order to allow a zero-argument version, a default value for zero arguments has to be specified.
        The default value should be a neutral element of this operation (op default x) and (op x default) should return x in this case.
        This condition is not checked, but may be useful in many cases.
        (if useful, it will be checked in future versions)
    inline (a ...)
        static-if (none? a)
            if (none? default)
                static-error "Operation requires at least one argument"
            else default
        elseif (va-empty? ...)
            a
        else
            op a ...

let + = 
    make-argless (do +)

let * = 
    make-argless (do *)

let & = 
    make-argless (do &)

let | = 
    make-argless (do |)

let .. = 
    make-argless (do ..)

locals;

