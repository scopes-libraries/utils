using import .va

inline curry (f now...)
    inline (later...)
        f ((va-join now...) later...)

inline compose (funs...)
    inline (args...)
        va-rfold none
            inline (k f result...)
                static-if (none? (va@ 0 result...))
                    f args...
                else
                    f result...
            funs...

inline compose-apply (op f...)
    fn (args...)
        op
            (va-map-functions f...) args...

inline complement (...)
    compose (inline (x) (not x)) ...

inline value (val)
    inline (...)
        val

inline all (pred ...)
    va-rfold true
        inline "#hidden" (key value)
            assert (key == unnamed)
            if (not (pred value))
                return false
            true
        ...

inline any (pred ...)
    va-rfold false
        inline "#hidden" (key value)
            assert (key == unnamed)
            if (pred value)
                return true
            false
        ...

inline fargs (args...)
    inline (f)
        f args...

inline count (pred seq args...)
    fold (counter = 0) for value in seq
        if (pred value args...)
            counter + 1
        else
            counter

locals;

