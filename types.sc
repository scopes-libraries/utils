using import .init
using import .va

inline generic-name (typename args...)
    let str =
        va-lfold none
            inline (key ty result)

                str := (tostring ty)
                let add =
                    static-if (none? result) str
                    else
                        .. result " " str
            args...
    .. typename "<" str ">"

sugar mono (name)
    qq [embed]
        [typedef name] : ([tuple])
            [inline] __typecall (ty)
                [bitcast] (tupleof) this-type
            [inline] __imply (vT ty)
                [static-if] (ty == [type])
                    inline (value) this-type
        [let name] = ([name])

run-stage;

mono VariantTypeError

@@ memo
inline variant (T...)
    let size =
        va-lfold none
            inline (k T r)
                let size = (sizeof T)
                static-if (none? r) size
                else (size * r)
            T...
    typedef (generic-name "variant" T...) : (tuple u16 (array u8 size))
        inline __typecall (ty value)
            let id =
                va-pos (typeof value) T...
            local value = value
            let value =
                bitcast &value (pointer (array u8 size))

            bitcast
                tupleof (u16 id) (load value)
                this-type

        inline is? (self T)
            let id =
                va-pos T T...
            (storagecast self) @ 0 == id

        inline get (self T)
            let id =
                va-pos T T...
            let self = (storagecast self)
            if (self @ 0 == id)
                local value =
                    self @ 1
                let value =
                    bitcast &value (pointer T)
                load value
            else
                raise VariantTypeError


@@ memo
inline proxy (supertype storage)
    typedef (generic-name (tostring supertype) storage) < supertype :: (tuple storage)
        inline value (object)
            (storagecast object) @ 0
        inline __repr (object)
            repr
                value object

inline proxyof (supertype value)
    bitcast (tupleof value) (proxy supertype (typeof value))

sugar proxydef (name body...)
    qq embed
        [typedef] [name] (unquote-splice body...)
        [typedef+] [name]
            [inline] __typecall (this value)
                [static-if] (this == this-type)
                    [proxyof] this value
                else
                    [static-error] "Typecalling proxy types is not possible for subtypes"
            [inline] type (storage)
                [proxy] this-type storage

let generic-name mono

locals;
