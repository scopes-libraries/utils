using import itertools
using import spicetools

fn va-reverse (arg args...)
    va-lfold arg
        inline (key value result...)
            _ value result...
        args...

inline va-map-functions (f...)
    inline (args...)
        va-map
            inline (f)
                f args...
            f...

spice va-range (args...)
    let start end step =
        spice-match args...
        case (start as i32, end as i32, step as i32,)
            _ start end step
        case (start as i32, end as i32, )
            _ start end 1
        case (end as i32,)
            _ 0 end 1
        default
            error "Unsupported pattern"
    sc_argument_list_map_new ((end - start) // step)
        inline (i)
            start + i * step

inline va-type@ (T args...)
    va-lfold none
        inline "#hidden" (key value)
            static-if ((typeof value) <= T)
                return value
        args...

spice va-fill (count arg)
    sc_argument_list_map_new (count as i32)
        inline (i) arg

inline va-pos (val args...)
    va-lifold none
        inline "#hidden" (i key arg result)
            static-if (not (none? result)) result
            elseif (arg == val) i
            else result
        args...

define select
    inline first (a b)
        static-if (none? a) b
        else a

    inline last (a b)
        static-if (none? b) a
        else b

    inline only-one (a b)
        static-if (none? a) b
        elseif (none? b) a
        else
            static-error "Only one is allowed"
    locals;

inline va-pos-finder (test select)
    inline va-pos-finder (val args...)
        va-lifold none
            inline (i key arg result)
                define next
                    static-if (test val arg) i
                select result next
            args...

inline va-find-pos (val test select args...)
    (va-pos-finder test select)
        val args...

inline va-finder (test select)
    inline va-pos-finder (val args...)
        va-lfold none
            inline (key arg result)
                define next
                    static-if (test val arg) arg
                select result next
            args...

inline va-find (val test select args...)
    (va-finder test select)
        val args...

spice va-rest (args...)
    let start rest... =
        spice-match args...
        case (start as i32, rest...)
            _ start rest...
        default
            error "Unsupported pattern"
    let end =
        'argcount rest...
    sc_argument_list_map_new (end - start)
        inline (i)
            spice-quote
                va@
                    start + i
                    rest...

spice va-slice (args...)
    let start end rest... =
        spice-match args...
        case (start as i32, end as i32, rest...)
            _ start end rest...
        default
            error "Unsupported pattern"
    sc_argument_list_map_new (end - start)
        inline (i)
            spice-quote
                va@
                    start + i
                    rest...

let scope1 = (locals)

run-stage;

inline va-mappend (f args...)
    let args... =
        va-rfold none
            inline (k v none result...)
                _ none
                    (va-join (f (key k v))) result...
            args...
    va-slice 1 (va-countof args...) args...

inline va-cycle (shift ...)
    let count =
        va-countof ...
    static-if (count == 0)
        return;
    shift := shift % count
    let shift =
        static-if (shift < 0) -shift
        else
            count - shift
    let first... = (va-rest shift ...)
    let rest... = (va-slice 0 shift ...)
    (va-join first...) rest...

inline va-swap (i j ...)
    let first... =
        va-slice 0 i ...
    let a second... =
        va-slice i j ...
    let b third... =
        va-rest j ...
    (va-join ((va-join first...) b second...)) a third...

#
    fn va-find (pred data...)
        let len = (va-countof data...)
        let loop (i) = 0
        if (i < len)
            let val = (va@ i data...)
            if (pred val)
                return i val
            loop (i + 1)
        len

    fn va-remove (pred data...)
        let len = (va-countof data...)
        let loop (i result...) = 0
        if (i < len)
            let val = (va@ i data...)
            if (pred val)
                loop (i + 1) result...
            else
                loop (i + 1) val result...
        result...

    fn va-remove-doubles (a b ...)
        if (not (and (none? a) (none? b)))
            return
                (va-join (if (a != b) a)) (recur b ...)

    fn va-group-counts (...)
        if (va-empty? ...)
        else
            let a ... = ...
            if (va-empty? ...)
                1
            else
                let first-count counts... =
                    va-group-counts ...
                if (a == ...)
                    _ (first-count + 1) counts...
                else
                    _ 1 first-count counts...

    fn va-all (pred first ...)
        if (va-empty? ...)
            pred first
        else
            and
                pred first
                va-all pred ...

    fn va-count (pred ...)
        let len = (va-countof ...)
        let loop (i count) = 0 0
        if (i < len)
            let new-count =
                (pred (va@ i ...)) as (typeof count)
            loop (i + 1) new-count
        count

    fn va-sort (f first data...)
        if (va-empty? data...)
            return first
        let pred =
            fn (val) (f first val)
        let sorted-data... =
            recur f data...
        let pos =
            va-find pred sorted-data...
        (va-join (va-slice 0 pos sorted-data...)) first (va@ pos sorted-data...)

    #fn va-sort (f data...)
        fn quicksort (data...)
            fn devide (data...)
                let left right = 0 ((va-countof data...) - 1)
                let pivot = (va@ right data...)
                let loop (i j data...) = left (right - 1) data...
                let inner (i) = i
                if ((f (va@ i data...) pivot) and (i < (right - 1)))
                    inner (i + 1)
                let inner (j) = j
                if ((f pivot (va@ j data...)) and (left < j))
                    inner (j + 1)
                if (< i j)
                    loop
                        va-swap i j data...
                return i (va-swap i right data...)
            let i data... =
                devide data...
            dump i data...
            let left... =
                va-slice 0 i data...
            let left... =
                quicksort left...
            let right... =
                quicksort (va@ i data...)
            (va-join left...) right...
        quicksort data...

scope1 .. (locals)

