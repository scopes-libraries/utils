spice combine-scopes (scope other)
    fold (new-scope = (other as Scope)) for k v in (scope as Scope)
        'bind new-scope k v

sugar calling-import (scope args...)
    qq embed
        import [scope]
        define [scope]
            [scope]
                unquote-splice args...

sugar do-file (filename)
    qq [do]
        unquote-splice
            (list-load (filename as string)) as list

sugar embed-file (filename)
    qq [embed]
        unquote-splice
            (list-load (filename as string)) as list

sugar use (filename)
    qq [do]
        unquote-splice
            (list-load (filename as string)) as list

run-stage;

typedef+ Scope
    inline __.. (T U)
        static-if (T == Scope and U == Scope)
            inline (a b)
                static-if (and (va-map constant? a b))
                    combine-scopes a b
                else
                    clone-scope-contents a b

sugar ign (...)
    """"Ignores all of it's arguments.
        Some other way for commenting, while still checking for correct syntax and matching of brackets.
    list do

sugar dbg (args...)
    cons _
        'reverse
            fold (result = '()) for arg in args...
                cons
                    list do
                        list print (tostring arg) "=" arg
                        arg
                    result

#
    #define-marco using-import-call

        list using
            list do
                list import name

    #fn sort (data)
        """"An unstable sort function.
        let loop (left right) = 0 ((countof data) - 1)

inline vector-map (fun vecs...)
    let vec = vecs...
    let count =
        (countof vec) as i32
    let type =
        elementof (typeof vec) 0
    vectorof type
        va-map
            inline (i)
                fun
                    va-map (fn (vec) (extractelement vec i)) vecs...
            va-range count
#

    #fn exposed? (method self)
        not
            none?
                forward-typeattr (typeof self) method

    #fn valid-call (method self ...)
        let value =
            forward-typeattr (typeof self) method
        if (not (none? value))
            value self ...

sugar keyed (args...)
    let expressions =
        loop (result arg rest... = '() (decons args...))
            if
                ('typeof arg) != Symbol
                error "Name has to be a symbol"
            let arg =
                list arg '= arg
            if (empty? rest...)
                break
                    cons arg result
            else
                _
                    cons arg result
                    decons rest...
    cons _ ('reverse expressions)

sugar method-fn (name)
    qq [fn] [name] (...)
        '[name] ...

sugar method-inline (name)
    qq [inline] [name] (...)
        '[name] ...

let calling-import do-file embed-file

fn pos (val d)
    let count = (countof val)
    for i in (range count)
        if ((val @ i) == d)
            return i
    return count

locals;

