using import .va

inline make-multi (f)
    """"Takes a function `f`, that takes one argument, and returns a function, that takes multiple arguments, that calls `f` on multiple arguments, and returns all values.
        
        ### Example:
        ```
        let not = (make-multi not) # define multiple value not
        
        let a b =
            not false true
        assert
            and
                (a == true)
                (b == false)
        ```
    inline (...)
        va-map f ...

let make-multis =
    make-multi make-multi

inline make-multi-function (f)
    inline (arg ...)
        va-rfold (_)
            inline (key value result...)
                _
                    f value
                    result...

let make-multi-functions =
    make-multi-function make-multi-function

locals;
