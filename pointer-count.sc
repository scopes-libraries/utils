using import struct

using import .init
using import .types

typedef PointerCount < CStruct
    @@ memo
    inline __typecall (ty T)
        struct (generic-name "PointerCount" T) < this-type
            value : (mutable pointer T)
            count : usize

            inline __rimply (NT)
                static-match NT
                case Nothing
                    inline ()
                        CStruct.__typecall this-type
                            value = null
                            count = 0
                case T
                    inline (v)
                        CStruct.__typecall this-type
                            value = &v
                            count = 1
                default
                    if (NT < array and (elementof NT) == T)
                    inline (v)
                        let count =
                            countof v
                        static-if (count == 0)
                            CStruct.__typecall this-type
                                value = null
                                count = 0
                        else
                            CStruct.__typecall this-type
                                value =
                                    & (v @ 0)
                                keyed count

            inline __unpack (self)
                _ self.value self.count

#inline PointerCount (T)
    struct (generic-name "PointerCount" T) plain
        value : (mutable pointer T)
        count : usize

locals;
