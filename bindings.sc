using import .casing
using import Array

inline prefix-remover (prefix)
    """"Removes `prefix` from `string`.
        Both arguments are strings.

        A second return value shows, if the string contains a valid prefix.
        A prefix is also invalid, if the prefix is not explicitely seperated from the string.

        This is useful for renaming imported symbols from c.

        The specified casing of `prefix` is not important.
        In case `prefix = "prefix"`, the following names will be converted this way:
        * prefix_name => name
        * prefixname invalid
        * prefixName => name
        * PrefixName => Name
        * PREFIX_NAME => NAME
        * PREFIXNAME invalid
    inline (string)

        let prefix-len =
            countof prefix
        let valid =
            and
                (uppercase (lslice string prefix-len)) == (uppercase prefix)
                or
                    not
                        lowercase? (string @ prefix-len)
                    uppercase? (lslice string prefix-len)
                not
                    and
                        uppercase? (rslice string prefix-len)
                        uppercase? (string @ prefix-len)

        if valid
            let start =
                if ((string @ prefix-len) == 95:i8)
                    prefix-len + 1
                else
                    prefix-len

            if (lowercase? (string @ 0))
                let c =
                    alloca i8

                c @ 0 =
                    lowercase (string @ start)
                ..
                    sc_string_new c 1:usize
                    rslice string (start + 1)
            else
                rslice string start
        else
            error "String does not contain expected prefix"

typedef+ Scope
    spice remove-prefix (scope prefix)
        fold (new-scope = (Scope)) for k v in (scope as Scope)
            let str = (k as Symbol as string)
            try
                let new-string = ((prefix-remover (prefix as string)) str)
                'bind new-scope (Symbol new-string) v
            except (error)
                new-scope

    spice __unpack (scope)
        local args : (Array Value)
        for k v in (scope as Scope)
            if (('typeof k) == Symbol)
                k := k as Symbol
                'append args
                    sc_keyed_new k v
        sc_argument_list_new
            (countof args) as i32
            & (args @ 0)

inline copy-bindings (self modifier new-scope)
    static-match (typeof self)
    case type
        let new-scope =
            static-if (none? new-scope) self
            else new-scope
        let symbols setter =
            'symbols self
            'set-symbol
        for k v in symbols
            let str = (k as Symbol as string)
            try
                let new-string = (modifier str)
                setter new-scope (Symbol new-string) v
            except (error)
        new-scope
    case Scope
        let new-scope =
            static-if (none? new-scope) self
            else new-scope
        let symbols setter = self 'bind
        local new-scope = new-scope
        for k v in symbols
            let str = (k as Symbol as string)
            try
                let new-string = (modifier str)

                new-scope =
                    setter new-scope (Symbol new-string) v
            except (error)
        deref new-scope
    default
        static-error "Bindings can only copied from a `type` or a `Scope`"

locals;

